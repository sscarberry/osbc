import sys

from osbc.gui import application as gui


if __name__ == "__main__":
    app = gui.Application()
    app.run(sys.argv)
