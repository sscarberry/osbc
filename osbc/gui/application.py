import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gio
from gi.repository import Gtk

from osbc.startup import init_osbc

class Application(Gtk.Application):
    def __init__(self):
        Gtk.Application.__init__(self,
                                application_id="os.bc",
                                flags=Gio.ApplicationFlags.FLAGS_NONE)
        init_osbc()

    def do_activate(self):
        pass
