import os
from osbc.utils import create_directory, create_config

def init_os():
    #TODO: check for operating system type.
    pass

def init_dirs():
    """
    Initialization for required directories.
    """
    dirs = []
    for dir in dirs:
        create_directory()

def init_config():
    """
    Initialization for the software configuration.
    """
    config = None
    if not config:
        create_config()

def init_osbc():
    init_os()
    init_dirs()
    init_config()
