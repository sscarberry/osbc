import os

def create_directory(path):
    """
    Attempts to create a directory if it does not exist.
    """
    if not path:
        return
    path = os.path.expanduser(path)
    if not os.path.exists(path):
        os.makedirs(path)
    return path

def create_config():
    """
    Creates the standard software configuration.
    """
    config = {
        "income_sources": [
            {
                "name": "My Job",
                "amount": 3000.54,
                "frequency": "BI_WEEKLY"
            },
            {
                "name": "Rental 1",
                "amount": 1200.00,
                "frequency": "MONTHLY"
            }
        ],
        "transactions": [
            {
                "name": "Medical Bill",
                "amount": 80.00,
                "frequency": "MONTHLY",
                "count": 15
                #TODO Add more fields?
            }
        ]
    }

    #TODO Write the config here.
    pass
